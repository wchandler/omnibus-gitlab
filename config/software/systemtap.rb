#
# copyright 2012-2014 chef software, inc.
#
# licensed under the apache license, version 2.0 (the "license");
# you may not use this file except in compliance with the license.
# you may obtain a copy of the license at
#
#     http://www.apache.org/licenses/license-2.0
#
# unless required by applicable law or agreed to in writing, software
# distributed under the license is distributed on an "as is" basis,
# without warranties or conditions of any kind, either express or implied.
# see the license for the specific language governing permissions and
# limitations under the license.
#

name 'systemtap'
default_version '3.3'

# runtime dependency
dependency 'bison'
dependency 'elfutils'
dependency 'flex'
dependency 'gettext'

license 'GPL-3.0'
license_file 'COPYING'

source url: "https://sourceware.org/systemtap/ftp/releases/systemtap-#{version}.tar.gz",
       sha256: '1fcbe2e39b82f27a788d6060b47045ca24a476c149d20d925253ba2a382a64f9'


relative_path "systemtap-#{version}"

build do
  env = with_standard_compiler_flags(with_embedded_path)

  configure_command = [
    "MSGFMT=#{install_dir}/embedded/bin/msgfmt",
    './configure',
    "--prefix=#{install_dir}/embedded",
    "--with-elfutils=#{install_dir}/embedded/share/elfutils",
  ]

  command configure_command.join(' '), env: env

  make "-j #{workers}", env: env
  make 'install', env: env
end
