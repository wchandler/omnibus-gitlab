#
# Copyright 2012-2014 Chef Software, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

name 'elfutils'
default_version '0.173'

license 'MIT'
license_file 'COPYING'

source url: "ftp://sourceware.org/pub/elfutils/#{version}/elfutils-#{version}.tar.bz2",
       sha256: 'b76d8c133f68dad46250f5c223482c8299d454a69430d9aa5c19123345a000ff'


relative_path "elfutils-#{version}"

build do
  mkdir "#{install_dir}/embedded/share/elfutils"
  copy '*', "#{install_dir}/embedded/share/elfutils"
end
