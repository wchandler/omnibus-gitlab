#
# copyright 2012-2014 chef software, inc.
#
# licensed under the apache license, version 2.0 (the "license");
# you may not use this file except in compliance with the license.
# you may obtain a copy of the license at
#
#     http://www.apache.org/licenses/license-2.0
#
# unless required by applicable law or agreed to in writing, software
# distributed under the license is distributed on an "as is" basis,
# without warranties or conditions of any kind, either express or implied.
# see the license for the specific language governing permissions and
# limitations under the license.
#

name 'bison'
default_version '3.0.5'

# runtime dependency
dependency 'm4'

license 'GPL-3.0'
license_file 'COPYING'

source url: "http://ftp.gnu.org/gnu/bison/bison-#{version}.tar.gz",
       sha256: 'cd399d2bee33afa712bac4b1f4434e20379e9b4099bce47189e09a7675a2d566'


relative_path "bison-#{version}"

build do
  env = with_standard_compiler_flags(with_embedded_path)

  configure_command = [
    './configure',
    "--prefix=#{install_dir}/embedded",
  ]

  command configure_command.join(' '), env: env

  make "-j #{workers}", env: env
  make 'install', env: env
end
